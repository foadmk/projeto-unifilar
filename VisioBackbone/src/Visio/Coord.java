package Visio;

public class Coord {
	private int x;
	private int y;

	public Coord() {
		super();
	}

	public Coord(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	
	public void imprimir() {
		System.out.println("X = " + getX());
		System.out.println("Y = " + getY());		
	}

}
