package Visio;

public class Pop extends PontoGeo {
	
	private String sigla;
	private String tipo;

	public Pop() {
	}
	
	public Pop(String sigla, Coord coordenada, String endereco, String tipo) {
		super(coordenada, endereco);
		this.tipo = tipo;
		this.sigla = sigla;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void imprimir() {
		System.out.println("Sigla: "+sigla+"-"+tipo);
		super.imprimir();
	}
	
	
}
