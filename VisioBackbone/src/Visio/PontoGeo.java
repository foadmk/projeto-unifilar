package Visio;

import java.util.ArrayList;

public class PontoGeo {

	private Coord coordenada;
	private String endereco;
	ArrayList<Cabo> refCabos = new ArrayList<Cabo>();
	
	public void associarCabo(Cabo cabo) {
		refCabos.add(cabo);
	}

	public ArrayList<Cabo> cabosAssociados() {
		return refCabos;
	}

	public boolean verificaCaboAssociado(Cabo cabo) {
		return refCabos.contains(cabo);
	}	

	public PontoGeo(Coord coordenada) {
		super();
		this.coordenada = coordenada;
	}
	

	public PontoGeo(Coord coordenada, String endereco) {
		super();
		this.coordenada = coordenada;
		this.endereco = endereco;
	}


	public PontoGeo() {
		super();
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Coord getCoordenada() {
		return coordenada;
	}

	public void setCoordenada(Coord coordenada) {
		this.coordenada = coordenada;
	}

	public void imprimir() {
			
		System.out.println("Endere�o: " + endereco);
		
		coordenada.imprimir();
	}

}
