package Visio;

public class Teste {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Pop A = new Pop("ADR",new Coord(1,2),"Rua xx, 40", "SE");
		Ceo B = new Ceo("54",new Coord(2,3),"Rua xx, 40", "OPGW");
		
		Cabo C = new Cabo("OPOA-01", A, B, 10.2, 5.1, 36);
		
		C.setNome("OPOA-02");
		
		A.imprimir();
		B.imprimir();
		C.imprimir();
		
		System.out.println("_________________Cabos___________________");
		for(Cabo X : A.cabosAssociados()){
			X.imprimir();
		}
				

		
	}

}
