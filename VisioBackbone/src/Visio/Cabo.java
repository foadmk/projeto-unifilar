package Visio;

public class Cabo {

	private PontoGeo pontaA;
	private PontoGeo pontaB;

	private String nome;
	private double comprimentoTotal;
	private double comprimentoTrecho;

	private int noFibras;

	public Cabo(String nome, PontoGeo pontaA, PontoGeo pontaB,
			double comprimentoTotal, double comprimentoTrecho, int noFibras) {
		super();
		
		pontaA.associarCabo((Cabo)this);
		pontaB.associarCabo((Cabo)this);
		this.pontaA = pontaA;
		this.pontaB = pontaB;
		this.nome = nome;
		this.comprimentoTotal = comprimentoTotal;
		this.comprimentoTrecho = comprimentoTrecho;
		this.noFibras = noFibras;
	}

	public PontoGeo getPontaA() {
		return pontaA;
	}

	public void setPontaA(PontoGeo pontaA) {
		this.pontaA = pontaA;
	}

	public PontoGeo getPontaB() {
		return pontaB;
	}

	public void setPontaB(PontoGeo pontaB) {
		this.pontaB = pontaB;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getComprimentoTotal() {
		return comprimentoTotal;
	}

	public void setComprimentoTotal(double comprimentoTotal) {
		this.comprimentoTotal = comprimentoTotal;
	}

	public double getComprimentoTrecho() {
		return comprimentoTrecho;
	}

	public void setComprimentoTrecho(double comprimentoTrecho) {
		this.comprimentoTrecho = comprimentoTrecho;
	}

	public int getNoFibras() {
		return noFibras;
	}

	public void setNoFibras(int noFibras) {
		this.noFibras = noFibras;
	}
	
	public void imprimir() {
		System.out.println("Cabo: "+nome+" ("+comprimentoTotal+" km - "+noFibras+" F.O.)");
		System.out.println("Trecho: "+comprimentoTrecho+" km");
		System.out.println("Ponta A");
		pontaA.imprimir();
		System.out.println("Ponta B");
		pontaB.imprimir();
		
		
	}


}
