package Visio;

import java.util.ArrayList;

public class Ceo extends PontoGeo {

	private String numero;
	private String tipo;


	public Ceo(String numero, Coord coordenada, String tipo) {
		super(coordenada);
		this.numero = numero;
		this.tipo = tipo;
	}

	public Ceo(String numero, Coord coordenada, String endereco, String tipo) {
		super(coordenada, endereco);
		this.numero = numero;
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}



	public void imprimir() {
		System.out.println("CEO: " + numero);
		super.imprimir();
	}

}
