package database;

import java.sql.Timestamp;
import java.util.Calendar;

import objetos.Circuito;
import objetos.FibraOptica;

public class dbOcupacao extends BancoDados {

	public static final String DB = "ocupacao.db";
	public static final String TABLE = "OCUPACAO";

	public static final int ID = 0;
	public static final int IDFIBRA = 1;
	public static final int NOME = 2;
	public static final int ATIVO = 3;
	public static final int TS = 4;
	public static final int QUANT = 5;

	long index; // still set automatically
	public String idFibra;
	public String nome;
	public boolean ativo;
	Timestamp ts;
	
	
	public void populate(FibraOptica fibra, Circuito circuito) {

		idFibra = fibra.getIdFibra();
		nome = circuito.getIdentificador();
		ativo = circuito.isAtivo();

	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case IDFIBRA:
			return "IDFIBRA";
		case NOME:
			return "NOME";
		case ATIVO:
			return "ATIVO";
		case TS:
			return "TS";
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER 	PRIMARY KEY     AUTOINCREMENT	NOT NULL";
		case IDFIBRA:
			return "TEXT    	NOT NULL";
		case NOME:
			return "TEXT    	NOT NULL";
		case ATIVO:
			return "BOOLEAN    	NOT NULL";
		case TS:
			return "DATE UNIQUE NOT NULL";
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case ID:
			return SQLite.toText(index);
		case IDFIBRA:
			return SQLite.toText(idFibra);
		case NOME:
			return SQLite.toText(nome);
		case ATIVO:
			return SQLite.toText(ativo);
		case TS:
			return SQLite.toText(ts);
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

	@Override
	public int getIndexIdentifier() {
		return ID;
	}
	
	@Override
	public void updateTS() {
		Calendar t = Calendar.getInstance();
		ts = new Timestamp(t.getTimeInMillis());		
	}
}
