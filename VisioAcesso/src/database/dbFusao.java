package database;

import java.sql.Timestamp;
import java.util.Calendar;

import objetos.FusaoOptica;
import objetos.TermCabo;

public class dbFusao extends BancoDados {
	 
	public static final String DB="fusoes.db";
	public static final String TABLE="FUSOES";
	
	public static final int ID=0;
	public static final int IDTERMINAL=1;
	public static final int IDCABOA=2;
	public static final int IDCABOB=3;
	public static final int FIBRAA=4;
	public static final int FIBRAB=5;
	public static final int TS=6;
	
	public static final int QUANT=7;
	
	public long index;
	public String idTerminal;
	public String idCaboA;
	public String idCaboB;
	public int fibraA;
	public int fibraB;	
	Timestamp ts;
	
	public void populate(TermCabo terminal, FusaoOptica fusao) {
		idTerminal =terminal.getIdTerm();
		fibraA = fusao.getFibraA().getNumero();
		fibraB = fusao.getFibraB().getNumero();
		idCaboA = fusao.getFibraA().getCaboPai().getIdCabo();
		idCaboB = fusao.getFibraB().getCaboPai().getIdCabo();
	}
	
	@Override
	public String sqlName(int var){
		switch (var){
		case ID:		return "ID";
		case IDTERMINAL:return "IDTERMINAL";
		case IDCABOA:	return "IDCABOA";
		case IDCABOB:	return "IDCABOB";
		case FIBRAA:	return "FIBRAA";
		case FIBRAB:	return "FIBRAB";
		case TS:		return "TS";
		}
		return null;
	}
	
	@Override
	public String sqlType(int var){
		switch (var){
		case ID:			return "INTEGER 	PRIMARY KEY     AUTOINCREMENT	NOT NULL";
		case IDTERMINAL:	return "TEXT    	NOT NULL";
		case IDCABOA:		return "TEXT    	NOT NULL";
		case IDCABOB:		return "TEXT    	NOT NULL";
		case FIBRAA:		return "INTEGER     NOT NULL";
		case FIBRAB:		return "INTEGER     NOT NULL";
		case TS:			return "DATE UNIQUE NOT NULL";
		}
		return null;
	}
	
	@Override
	public String sqlValue(int var){
		switch (var){
		case ID:			return SQLite.toText(index);
		case IDTERMINAL:	return SQLite.toText(idTerminal);
		case IDCABOA:		return SQLite.toText(idCaboA);
		case IDCABOB:		return SQLite.toText(idCaboB);
		case FIBRAA:		return SQLite.toText(fibraA);
		case FIBRAB:		return SQLite.toText(fibraB);
		case TS:			return SQLite.toText(ts);
		}
		return null;
	}
	
	@Override
	public int sqlQuant() {
		return QUANT;
	}
	
	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

	@Override
	public int getIndexIdentifier() {
		return ID;
	}

	@Override
	public void updateTS() {
		Calendar t = Calendar.getInstance();
		ts = new Timestamp(t.getTimeInMillis());		
	}
}
