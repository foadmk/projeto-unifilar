package database;

import java.sql.Timestamp;
import java.util.Calendar;

import objetos.TermCabo;

public class dbTerminal extends BancoDados {

	public static final String DB = "terminacoes.db";
	public static final String TABLE = "TERMINACOES";

	public static final int ID = 0;
	public static final int IDTERMINAL = 1;
	public static final int UTMX = 2;
	public static final int UTMY = 3;
	public static final int ENDERECO = 4;
	public static final int BAIRRO = 5;
	public static final int CIDADE = 6;
	public static final int TS = 7;

	public static final int QUANT = 8;

	public long index;
	public String idTerminal;
	public int utmX;
	public int utmY;
	public String endereco;
	public String bairro;
	public String cidade;
	public Timestamp ts;

	public void populate(TermCabo terminal) {
		idTerminal = terminal.getIdTerm();
		utmX = terminal.getUtmX();
		utmY = terminal.getUtmY();
		endereco = terminal.getEndereco();
		endereco = terminal.getBairro();
		endereco = terminal.getCidade();
	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case IDTERMINAL:
			return "IDTERMINAL";
		case UTMX:
			return "UTMX";
		case UTMY:
			return "UTMY";
		case ENDERECO:
			return "ENDERECO";
		case BAIRRO:
			return "BAIRRO";
		case CIDADE:
			return "CIDADE";
		case TS:
			return "TS";
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER PRIMARY KEY     AUTOINCREMENT	NOT NULL";
		case IDTERMINAL:
			return "TEXT	NOT NULL";
		case UTMX:
			return "INTEGER NOT NULL";
		case UTMY:
			return "INTEGER NOT NULL";
		case ENDERECO:
			return "STRING  NOT NULL";
		case BAIRRO:
			return "STRING  NOT NULL";
		case CIDADE:
			return "STRING  NOT NULL";
		case TS:
			return "DATE  UNIQUE	NOT NULL";
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case IDTERMINAL:
			return SQLite.toText(idTerminal);
		case UTMX:
			return SQLite.toText(utmX);
		case UTMY:
			return SQLite.toText(utmY);
		case ENDERECO:
			return SQLite.toText(endereco);
		case BAIRRO:
			return SQLite.toText(bairro);
		case CIDADE:
			return SQLite.toText(cidade);
		case TS:
			return SQLite.toText(ts);
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

	@Override
	public int getIndexIdentifier() {
		return ID;
	}
	
	@Override
	public void updateTS() {
		Calendar t = Calendar.getInstance();
		ts = new Timestamp(t.getTimeInMillis());		
	}
}
