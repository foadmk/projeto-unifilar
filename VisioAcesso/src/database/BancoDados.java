package database;

import java.sql.SQLException;
import java.text.ParseException;

import javax.sql.rowset.CachedRowSet;

public abstract class BancoDados {

	public abstract int getIndexIdentifier();
	
	public abstract int sqlQuant();

	public abstract String dbName();

	public abstract String tableName();

	public abstract String sqlName(int index);

	public abstract String sqlType(int index);

	public abstract String sqlValue(int index);
	
	public abstract void updateTS();

	public String createTableString() {
		String sql = "CREATE TABLE IF NOT EXISTS " + tableName() + " (";
		for (int i = 0; i < sqlQuant(); i++) {
			sql += sqlName(i)+" "+sqlType(i);
			
			if((i+1)==sqlQuant()){
				sql +=")";
			}
			else{
				sql +=",";
			}
		}

		return sql;
	}

	public String insertString() {
		String sql = "INSERT INTO " + tableName() + " (";
		for (int i = 1; i < sqlQuant(); i++) {
			sql += sqlName(i);
			
			if((i+1)==sqlQuant()){
				sql +=")";
			}
			else{
				sql +=",";
			}
		}
		sql += "VALUES (";
		for (int i = 1; i < sqlQuant(); i++) {
			sql += sqlValue(i);
			
			if((i+1)==sqlQuant()){
				sql +=")";
			}
			else{
				sql +=",";
			}
		}

		return sql;
	}
	
	public String updateString() {
		String sql = "UPDATE " + tableName() + " SET";
		for (int i = 1; i < sqlQuant(); i++) {
			sql += sqlName(i) + "=" + sqlValue(i);
			
			if((i+1)!=sqlQuant()){
				sql +=",";
			}
		}
		sql += "WHERE INDEX=" + sqlValue(getIndexIdentifier());

		return sql;
	}
	
	public String selectString() {
		String sql = "SELECT * FROM " + tableName() + " WHERE ";
		return sql;
	}
	
	public String selectString(int varIdentifier, String comparison) {
		String sql = "SELECT * FROM " + tableName() + " WHERE ";
			sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}
	
	public String selectStringComp(int varIdentifier, String comparison) {
		String sql = "";
			sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}
	
	public String selectStringAnd(int varIdentifier, String comparison) {
		String sql = " AND";
			sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}
	
	public String selectStringOR(int varIdentifier, String comparison) {
		String sql = " OR";
			sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}
	
	public void saveRecord(){
		updateTS();
		SQLite.executeSQL(dbName(), insertString());
	}
	
	public CachedRowSet queryRecords(String query) throws ParseException, SQLException{
		return SQLite.selectSQL(dbName(), query);
	}

}
