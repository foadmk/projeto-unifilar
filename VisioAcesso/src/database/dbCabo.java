package database;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Vector;

import javax.sql.rowset.CachedRowSet;

import objetos.CaboOptico;
import objetos.Visio;


public class dbCabo extends BancoDados {
	
	public static final String DB="cabos.db";
	public static final String TABLE="CABOS";
	
	public static final int ID=0;
	public static final int IDCABO=1;
	public static final int VISIO=2;
	public static final int QUANTFIBRAS=3;
	public static final int COMPRIMENTO=4;
	public static final int IDTERMINALA=5;
	public static final int IDTERMILALB=6;
	public static final int TS=7;
	
	public static final int QUANT=8;
	
	public long index;
	public String idCabo;
	public String visio;
	public int quantFibras;
	public int comprimento;
	public String idTerminalA;
	public String idTerminalB;
	Timestamp ts;
	
	

	public dbCabo(long index, String idCabo, String visio, int quantFibras,
			int comprimento, String idTerminalA, String idTerminalB,
			Timestamp ts) {
		super();
		this.index = index;
		this.idCabo = idCabo;
		this.visio = visio;
		this.quantFibras = quantFibras;
		this.comprimento = comprimento;
		this.idTerminalA = idTerminalA;
		this.idTerminalB = idTerminalB;
		this.ts = ts;
	}

	public void populate(Visio visio, CaboOptico cabo) {

		idCabo = cabo.getIdCabo();
		this.visio = visio.getNome();
		comprimento = cabo.getComprimento();
		quantFibras = cabo.getFibras().size();
		idTerminalA = cabo.getPontaA().getIdTerm();
		idTerminalB = cabo.getPontaB().getIdTerm();
	}
	
	public Vector<dbCabo> getAll(Visio visio) throws ParseException, SQLException {
		
		String query = selectString(VISIO, visio.getNome());
		
		CachedRowSet results = queryRecords(query);
		
		return parseCachedRowSet(results);
	}
	
	public Vector<dbCabo> parseCachedRowSet(CachedRowSet rs)
			throws SQLException, ParseException {

		Vector<dbCabo> results = new Vector<dbCabo>();

		while (rs.next()) {
			dbCabo novoItem = new dbCabo(	rs.getLong(sqlName(ID)), 
											rs.getString(sqlName(IDCABO)),
											rs.getString(sqlName(VISIO)),
											rs.getInt(sqlName(QUANTFIBRAS)),
											rs.getInt(sqlName(COMPRIMENTO)),
											rs.getString(sqlName(IDTERMINALA)),
											rs.getString(sqlName(IDTERMILALB)),
											rs.getTimestamp(sqlName(TS)));
			results.add(novoItem);
		}
		return results;
	}
	
	@Override
	public String sqlName(int var){
		switch (var){
		case ID:			return "ID";
		case IDCABO:		return "IDCABO";
		case VISIO:			return "VISIO";
		case QUANTFIBRAS:	return "QUANTFIBRAS";
		case COMPRIMENTO:	return "COMPRIMENTO";
		case IDTERMINALA:	return "IDTERMINALA";
		case IDTERMILALB:	return "IDTERMILALB";
		case TS:			return "TS";
		}
		return null;
	}
	
	@Override
	public String sqlType(int var){
		switch (var){
		case ID:				return "INTEGER 	PRIMARY KEY     AUTOINCREMENT	NOT NULL";
		case IDCABO:			return "TEXT 	NOT NULL";
		case VISIO:				return "TEXT    NOT NULL";
		case QUANTFIBRAS:		return "INTEGER NOT NULL";
		case COMPRIMENTO:		return "INTEGER NOT NULL";
		case IDTERMINALA:		return "STRING  NOT NULL";
		case IDTERMILALB:		return "STRING  NOT NULL";
		case TS:				return "DATE UNIQUE NOT NULL";
		}
		return null;
	}
	
	@Override
	public String sqlValue(int var){
		switch (var){
		case ID:				return SQLite.toText(index);
		case IDCABO:			return SQLite.toText(idCabo);
		case VISIO:				return SQLite.toText(visio);
		case QUANTFIBRAS:		return SQLite.toText(quantFibras);
		case COMPRIMENTO:		return SQLite.toText(comprimento);
		case IDTERMINALA:		return SQLite.toText(idTerminalA);
		case IDTERMILALB:		return SQLite.toText(idTerminalB);
		case TS:				return SQLite.toText(ts);
		}
		return null;
	}
	
	
	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}
	
	@Override
	public int getIndexIdentifier() {
		return ID;
	}
	
	@Override
	public void updateTS() {
		Calendar t = Calendar.getInstance();
		ts = new Timestamp(t.getTimeInMillis());		
	}

}
