package exception;

public class caboNaoExiste extends Exception {

	private static final long serialVersionUID = -3363941247510764687L;
	
	public caboNaoExiste() { super(); }
	  public caboNaoExiste(String message) { super(message); }
	  public caboNaoExiste(String message, Throwable cause) { super(message, cause); }
	  public caboNaoExiste(Throwable cause) { super(cause); }
	}