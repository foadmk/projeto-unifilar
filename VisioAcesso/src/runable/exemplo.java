package runable;

import java.util.Vector;

import objetos.Circuito;
import objetos.FibraOptica;
import objetos.FusaoOptica;
import objetos.TermCabo;
import objetos.TrechoCabo;
import objetos.Visio;
import database.SQLite;
import database.dbCabo;
import database.dbCircuito;
import database.dbFusao;
import database.dbTerminal;

public class exemplo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Visio visio = new Visio("SE MCS");
		
		TermCabo semcs = new TermCabo();

		semcs.setNome("SE MCS");
		semcs.setTipo("POP");

		TermCabo ceo3090 = new TermCabo();

		ceo3090.setNome("3090");
		ceo3090.setTipo("CEO");
		
		TermCabo ceo3091 = new TermCabo();

		ceo3091.setNome("3091");
		ceo3091.setTipo("CEO");

		TrechoCabo mcs0002tr01 = new TrechoCabo(48);

		mcs0002tr01.setComprimento(443);
		mcs0002tr01.setNome("MCS0002");
		mcs0002tr01.setTrecho("01");

		mcs0002tr01.setPontaA(semcs);
		mcs0002tr01.setPontaB(ceo3090);

		
		TrechoCabo mcs0002tr02 = new TrechoCabo(48);

		mcs0002tr02.setComprimento(400);
		mcs0002tr02.setNome("MCS0002");
		mcs0002tr02.setTrecho("02");

		mcs0002tr02.setPontaA(ceo3090);
		mcs0002tr02.setPontaB(ceo3091);

		Vector<FusaoOptica> fusoes  = new Vector<FusaoOptica>();
		
		for(int i=0; i<48; i++){
			FusaoOptica fusao = new FusaoOptica();
			
			
			fusao.setTrechoCaboA(mcs0002tr01);
			fusao.setTrechoCaboB(mcs0002tr02);
			fusao.setFibraA(mcs0002tr01.getFibra(i));
			fusao.setFibraB(mcs0002tr02.getFibra(i));
			
			fusoes.add(fusao);
		}
		
		ceo3090.setFusoes(fusoes);
		Vector<FusaoOptica> sfusoes  = new Vector<FusaoOptica>();
		ceo3091.setFusoes(sfusoes);
		semcs.setFusoes(sfusoes);
		
		dbCabo cabo1 = new dbCabo();
		cabo1.populate(visio, mcs0002tr01);
		
		dbCabo cabo2 = new dbCabo();
		cabo2.populate(visio, mcs0002tr02);
		
		dbTerminal pop = new dbTerminal();
		pop.populate(semcs);

		dbTerminal ceo1 = new dbTerminal();
		ceo1.populate(ceo3090);
		
		dbTerminal ceo2 = new dbTerminal();
		ceo2.populate(ceo3091);
		
		dbFusao fusao = new dbFusao();

		dbCircuito circuito = new dbCircuito();
		
		
		SQLite.executeSQL(dbCabo.DB, cabo1.createTableString());
		SQLite.executeSQL(dbFusao.DB, fusao.createTableString());
		SQLite.executeSQL(dbTerminal.DB, pop.createTableString());
		SQLite.executeSQL(dbCircuito.DB, circuito.createTableString());
		
		cabo1.saveRecord();
		cabo2.saveRecord();
		ceo1.saveRecord();
		ceo2.saveRecord();
		pop.saveRecord();
		
		
		for(FusaoOptica x : fusoes){
			fusao.populate(ceo3091, x);
			fusao.saveRecord();
		}
		

		
	}
}
