package objetos;

import java.util.Vector;

public class Ocupacao {

	protected String IdFibra;
	protected Vector<Circuito> circuitos;

	public Vector<Circuito> getCircuitos() {
		return circuitos;
	}

	public void setCircuitos(Vector<Circuito> circuitos) {
		this.circuitos = circuitos;
	}

	public void addCircuito(Circuito circuito) {
		this.circuitos.add(circuito);
	}

	public void delCircuito(Circuito circuito) {
		this.circuitos.remove(circuito);

	}

	public String getIdFibra() {
		return IdFibra;
	}

	public void setIdFibra(String idFibra) {
		IdFibra = idFibra;
	}

}
