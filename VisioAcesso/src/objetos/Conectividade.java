package objetos;

import java.util.Vector;

public class Conectividade {

	protected String IdTerminacao;
	
	protected Vector<FusaoOptica> fusoes;
	
	public String getIdTerminacao() {
		return IdTerminacao;
	}
	public void setIdTerminacao(String idTerminacao) {
		IdTerminacao = idTerminacao;
	}
	public Vector<FusaoOptica> getFusoes() {
		return fusoes;
	}
	public void setFusoes(Vector<FusaoOptica> fusoes) {
		this.fusoes = fusoes;
	}
	
	
	
}
