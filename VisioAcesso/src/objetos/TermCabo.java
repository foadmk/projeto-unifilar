package objetos;

import java.util.Vector;

import exception.caboNaoExiste;

public class TermCabo extends PontoRelevante {

	private Vector<CaboOptico> cabosAssociados;

	private String tipo;

	private Vector<FusaoOptica> fusoes;
	
	

	public TermCabo() {
		cabosAssociados = new Vector<CaboOptico>();
		fusoes = new  Vector<FusaoOptica>();
		tipo = "NDF";
	}

	public String getIdTerm() {
		return tipo+"."+getNome();
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Vector<FusaoOptica> getFusoes() {
		return fusoes;
	}

	public void setFusoes(Vector<FusaoOptica> fusoes) {
		this.fusoes = fusoes;
	}

	public Vector<CaboOptico> getCabosAssociados() {
		return cabosAssociados;
	}

	public void setCabosAssociados(Vector<CaboOptico> cabosAssociados) {
		this.cabosAssociados = cabosAssociados;
	}

	public void addCabo(CaboOptico Cabo) {
		if (!cabosAssociados.contains(Cabo)) {
			cabosAssociados.add(Cabo);
		}
	}

	public void delCabo(CaboOptico Cabo) throws caboNaoExiste {
		if (cabosAssociados.contains(Cabo)) {
			cabosAssociados.remove(Cabo);
		}
		else
		{
			throw new caboNaoExiste();
		}
	}

	public String getBairro() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCidade() {
		// TODO Auto-generated method stub
		return null;
	}

}
