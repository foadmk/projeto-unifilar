package objetos;


public class FibraOptica  {
	
	private int numero;
	private CaboOptico caboPai;

	public FibraOptica(int numero, CaboOptico caboPai) {
		this.numero = numero;
		this.caboPai = caboPai;
	}
	
	public FibraOptica() {
	}
	
	public String getIdFibra() {
		return caboPai.getIdCabo()+"."+getNumero();
	}
	public CaboOptico getCaboPai() {
		return caboPai;
	}

	public void setCaboPai(CaboOptico caboPai) {
		this.caboPai = caboPai;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	

}
