package objetos;

public class FusaoOptica {

	private FibraOptica fibraA, fibraB;

	public FusaoOptica() {
	}

	public FusaoOptica(FibraOptica fibraA,
			FibraOptica fibraB) {
		super();

		this.fibraA = fibraA;
		this.fibraB = fibraB;
	}


	public FibraOptica getFibraA() {
		return fibraA;
	}

	public void setFibraA(FibraOptica fibraA) {
		this.fibraA = fibraA;
	}

	public FibraOptica getFibraB() {
		return fibraB;
	}

	public void setFibraB(FibraOptica fibraB) {
		this.fibraB = fibraB;
	}


}
