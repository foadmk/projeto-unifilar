package objetos;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.sql.rowset.CachedRowSet;

import todo.Tarefa;

public class CaboOptico {
	
	protected Vector<FibraOptica> fibras;
	
	private String nome;

	private String trecho;

	private int comprimento;
	
	public String getIdCabo() {
		return nome + "." + trecho;
	}
	
	private TermCabo pontaA, pontaB;

	public CaboOptico(int numFibras) {
		fibras = new Vector<FibraOptica>();
		for(int i=0; i<numFibras; i++){
			fibras.addElement(new FibraOptica(i, this));
		}
	}

	public CaboOptico(int comprimento, Vector<FibraOptica> fibras) {
		this.fibras = fibras;
		this.comprimento = comprimento;
	}

	

	public CaboOptico(Vector<FibraOptica> fibras, String idCabo,
			int comprimento, TermCabo pontaA, TermCabo pontaB) {
		super();
		this.fibras = fibras;
		this.nome = nome;
		this.trecho = trecho;
		this.comprimento = comprimento;
		this.pontaA = pontaA;
		this.pontaB = pontaB;
	}

	public String getNome() {
		return nome;
	}

	public String getTrecho() {
		return trecho;
	}

	public void setTrecho(String trecho) {
		this.trecho = trecho;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TermCabo getPontaA() {
		return pontaA;
	}

	public void setPontaA(TermCabo pontaA) {
		pontaA.addCabo(this);
		this.pontaA = pontaA;
	}

	public TermCabo getPontaB() {
		return pontaB;
	}

	public void setPontaB(TermCabo pontaB) {
		pontaB.addCabo(this);
		this.pontaB = pontaB;
	}

	public int getComprimento() {
		return comprimento;
	}

	public void setComprimento(int comprimento) {
		this.comprimento = comprimento;
	}

	public Vector<FibraOptica> getFibras() {
		return fibras;
	}

	public FibraOptica getFibra(int index) {
		return fibras.get(index);
	}

	public void setFibra(int index, FibraOptica fibra) {
		fibras.set(index, fibra);
	}

	public void setFibras(Vector<FibraOptica> fibras) {
		this.fibras = fibras;
	}
	
	
}
