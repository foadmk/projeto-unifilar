package objetos;


public class Circuito {

	private String identificador;
	private boolean ativo;

	public Circuito() {

	}

	public Circuito(String identificador) {
		this.identificador = identificador;
		ativo = false;
	}

	public Circuito(String identificador, boolean ativo) {
		this.identificador = identificador;
		this.ativo = ativo;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	

}
