package objetos;

public class PontoRelevante {
	
	private int utmX, utmY;
	
	private String nome;
	
	private String endereco;

	public PontoRelevante() {
		utmX = 0;
		utmY = 0;
		nome = "Sem nome";
		endereco = "Sem endere�o";
	}
	
	public PontoRelevante(int utmX, int utmY, String nome, String endereco) {
		this.utmX = utmX;
		this.utmY = utmY;
		this.nome = nome;
		this.endereco = endereco;
	}

	public int getUtmX() {
		return utmX;
	}

	public void setUtmX(int utmX) {
		this.utmX = utmX;
	}

	public int getUtmY() {
		return utmY;
	}

	public void setUtmY(int utmY) {
		this.utmY = utmY;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	
}
