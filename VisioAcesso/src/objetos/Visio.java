package objetos;

import java.util.Vector;

public class Visio {

	private String nome;
	private Vector<TermCabo> terminacoes;
	private Vector<CaboOptico> trechos;

	
	
	
	public Visio(String nome) {
		this.nome = nome;
		
		
		//carregar trechos
		//carregar circuitos das fibras

		//carregar terminacoes dos trechos
		//carregar fusoes das terminacoes
		
	}

	public String getNome() {
		return nome;
	}

	public void setNomeCabo(String nome) {
		this.nome = nome;
	}

	public Vector<TermCabo> getTerminacoes() {
		return terminacoes;
	}

	public void setTerminacoes(Vector<TermCabo> terminacoes) {
		this.terminacoes = terminacoes;
	}

	public Vector<TrechoCabo> getTrechos() {
		return trechos;
	}

	public void setTrechos(Vector<TrechoCabo> trechos) {
		this.trechos = trechos;
	}

}
