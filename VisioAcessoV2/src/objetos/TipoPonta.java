package objetos;

public class TipoPonta {

	static String getTipo(int id) {

		switch (id) {
		case 1:
			return "Estacao";
		case 2:
			return "Caixa";
		case 3:
			return "Cliente";
		case 4:
			return "Outro Diagrama";			
		}
		
		return null;
	}
}
