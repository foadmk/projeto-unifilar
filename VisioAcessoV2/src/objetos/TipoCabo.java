package objetos;

public class TipoCabo {

	private long id;
	private String nome;
	private int numeroFibras;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumeroFibras() {
		return numeroFibras;
	}

	public void setNumeroFibras(int numeroFibras) {
		this.numeroFibras = numeroFibras;
	}

}
