package objetos;

public class Local {

	private long id;

	private int utmX, utmY;

	private String endereco, bairro, cidade;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getUtmX() {
		return utmX;
	}

	public void setUtmX(int utmX) {
		this.utmX = utmX;
	}

	public int getUtmY() {
		return utmY;
	}

	public void setUtmY(int utmY) {
		this.utmY = utmY;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

}
