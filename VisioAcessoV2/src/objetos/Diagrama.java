package objetos;

import java.util.Vector;

public class Diagrama {

	private long id;
	private String nome;
	Vector<String> idCabos; 
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Vector<String> getIdCabos() {
		return idCabos;
	}
	public void setIdCabos(Vector<String> idCabos) {
		this.idCabos = idCabos;
	}

	
	
}