package objetos;

public class Ocupacao {

	private long id;
	private long idCabo;
	private int fibra;
	private long idCliente;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdCabo() {
		return idCabo;
	}

	public void setIdCabo(long idCabo) {
		this.idCabo = idCabo;
	}

	public int getFibra() {
		return fibra;
	}

	public void setFibra(int fibra) {
		this.fibra = fibra;
	}

	public long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}

}
