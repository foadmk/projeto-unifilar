package objetos;

public class Cabo {

	private long id;
	private String nome;
	private String trecho;
	private int comprimento;
	private int idTipoCabo;

	private int idTipoPontaA;
	private int idPontaA;

	private int idTipoPontaB;
	private int idPontaB;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTrecho() {
		return trecho;
	}

	public void setTrecho(String trecho) {
		this.trecho = trecho;
	}

	public int getComprimento() {
		return comprimento;
	}

	public void setComprimento(int comprimento) {
		this.comprimento = comprimento;
	}

	public int getIdTipoCabo() {
		return idTipoCabo;
	}

	public void setIdTipoCabo(int idTipoCabo) {
		this.idTipoCabo = idTipoCabo;
	}

	public int getIdTipoPontaA() {
		return idTipoPontaA;
	}

	public void setIdTipoPontaA(int idTipoPontaA) {
		this.idTipoPontaA = idTipoPontaA;
	}

	public int getIdPontaA() {
		return idPontaA;
	}

	public void setIdPontaA(int idPontaA) {
		this.idPontaA = idPontaA;
	}

	public int getIdTipoPontaB() {
		return idTipoPontaB;
	}

	public void setIdTipoPontaB(int idTipoPontaB) {
		this.idTipoPontaB = idTipoPontaB;
	}

	public int getIdPontaB() {
		return idPontaB;
	}

	public void setIdPontaB(int idPontaB) {
		this.idPontaB = idPontaB;
	}

}
