package objetos;

public class Caixa {

	private long id;
	private String nome;
	private int posX, posY;
	private int idTipoCaixa;
	private int idLocal;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public int getIdTipo() {
		return idTipoCaixa;
	}

	public void setIdTipo(int idTipoCaixa) {
		this.idTipoCaixa = idTipoCaixa;
	}

	public int getIdLocal() {
		return idLocal;
	}

	public void setIdLocal(int idLocal) {
		this.idLocal = idLocal;
	}

}
