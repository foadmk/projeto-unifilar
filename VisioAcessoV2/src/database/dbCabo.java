package database;

import objetos.Cabo;

public class dbCabo extends bancoDados {

	public static final String DB = "visio.db";
	public static final String TABLE = "CABOS";

	public static final int ID = 0;
	public static final int NOME = 1;
	public static final int TRECHO = 2;
	public static final int COMPRIMENTO = 3;
	public static final int IDTIPOCABO = 4;
	public static final int IDTIPOPONTAA = 5;
	public static final int IDTIPOPONTAB = 6;
	public static final int IDPONTAA = 7;
	public static final int IDPONTAB = 8;
	public static final int QUANT = 9;


	public Cabo item;

	public void populate(Cabo cabo) {
		item = cabo;
	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case NOME:
			return "NOME";
		case TRECHO:
			return "TRECHO";
		case COMPRIMENTO:
			return "COMPRIMENTO";			
		case IDTIPOCABO:
			return "IDTIPOCABO";		
		case IDTIPOPONTAA:
			return "IDTIPOPONTAA";		
		case IDTIPOPONTAB:
			return "IDTIPOPONTAB";				
		case IDPONTAA:
			return "IDPONTAA";		
		case IDPONTAB:
			return "IDPONTAB";					
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER NOT NULL";
		case NOME:
			return "STRING NOT NULL";
		case TRECHO:
			return "STRING NOT NULL";
		case COMPRIMENTO:
			return "INTEGER NOT NULL";
		case IDTIPOCABO:
			return "INTEGER NOT NULL";			
		case IDTIPOPONTAA:
			return "INTEGER NOT NULL";		
		case IDTIPOPONTAB:
			return "INTEGER NOT NULL";	
		case IDPONTAA:
			return "INTEGER NOT NULL";	
		case IDPONTAB:
			return "INTEGER NOT NULL";				
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case ID:
			return SQLite.toText(item.getId());
		case NOME:
			return SQLite.toText(item.getNome());
		case TRECHO:
			return SQLite.toText(item.getTrecho());
		case COMPRIMENTO:
			return SQLite.toText(item.getComprimento());
		case IDTIPOCABO:
			return SQLite.toText(item.getIdTipoCabo());	
		case IDTIPOPONTAA:
			return SQLite.toText(item.getIdTipoPontaA());		
		case IDTIPOPONTAB:
			return SQLite.toText(item.getIdTipoPontaB());	
		case IDPONTAA:
			return SQLite.toText(item.getIdPontaA());	
		case IDPONTAB:
			return SQLite.toText(item.getIdPontaB());				
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

}
