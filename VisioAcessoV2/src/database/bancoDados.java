package database;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;

import javax.sql.rowset.CachedRowSet;

public abstract class bancoDados {

	
	public abstract int sqlQuant();

	public abstract String dbName();

	public abstract String tableName();

	public abstract String sqlName(int index);

	public abstract String sqlType(int index);

	public abstract String sqlValue(int index);

	long index;
	Timestamp ts;
	String usuario;
	boolean ativo;

	public long getIndexIdentifier(){
		return index;
	}
	
	public void updateTS() {
		Calendar t = Calendar.getInstance();
		ts = new Timestamp(t.getTimeInMillis());
	}
	

	public void updateUsuario() {
		usuario = dbLogin.getUsuario();
	}

	public String createTableString() {
		String sql = "CREATE TABLE IF NOT EXISTS " + tableName() + " (";

		sql += "INDEX INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
				+ "USUARIO STRING NOT NULL," + "TS DATE NOT NULL,"
				+ "ATIVO BOOLEAN NOT NULL,";

		for (int i = 0; i < sqlQuant(); i++) {
			sql += sqlName(i) + " " + sqlType(i);

			if ((i + 1) == sqlQuant()) {
				sql += ")";
			} else {
				sql += ",";
			}
		}

		return sql;
	}

	public String insertString() {
		String sql = "INSERT INTO " + tableName() + " (";

		sql += "USUARIO, TS, ATIVO,";

		for (int i = 1; i < sqlQuant(); i++) {
			sql += sqlName(i);

			if ((i + 1) == sqlQuant()) {
				sql += ")";
			} else {
				sql += ",";
			}
		}
		sql += "VALUES (";

		sql += SQLite.toText(usuario) + "," + SQLite.toText(ts) + ","
				+ SQLite.toText(ativo) + ",";

		for (int i = 1; i < sqlQuant(); i++) {
			sql += sqlValue(i);

			if ((i + 1) == sqlQuant()) {
				sql += ")";
			} else {
				sql += ",";
			}
		}

		return sql;
	}

	public String updateString() {
		String sql = "UPDATE " + tableName() + " SET ";
		for (int i = 1; i < sqlQuant(); i++) {
			sql += sqlName(i) + "=" + sqlValue(i);

			if ((i + 1) != sqlQuant()) {
				sql += ",";
			}
		}
		sql += " WHERE INDEX=" + SQLite.toText(getIndexIdentifier());

		return sql;
	}
	
	public String desativarString() {
		String sql = "UPDATE " + tableName() + " SET ATIVO = FALSE WHERE INDEX=" + SQLite.toText(getIndexIdentifier());
		return sql;
	}

	public String selectString() {
		String sql = "SELECT * FROM " + tableName() + " WHERE ";
		return sql;
	}

	public String selectString(int varIdentifier, String comparison) {
		String sql = "SELECT * FROM " + tableName() + " WHERE ";
		sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}
	
	public String selectActivoString(int varIdentifier, String comparison) {
		String sql = "SELECT * FROM " + tableName() + " WHERE ATIVO = TRUE AND ";
		sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}

	public String selectStringComp(int varIdentifier, String comparison) {
		String sql = "";
		sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}

	public String selectStringAnd(int varIdentifier, String comparison) {
		String sql = " AND ";
		sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}

	public String selectStringOR(int varIdentifier, String comparison) {
		String sql = " OR ";
		sql += sqlName(varIdentifier) + "=" + comparison;
		return sql;
	}

	public void saveRecord() {
		SQLite.executeSQL(dbName(), desativarString());
		updateTS();
		updateUsuario();
		SQLite.executeSQL(dbName(), insertString());
	}

	public CachedRowSet queryRecords(String query) throws ParseException,
			SQLException {
		return SQLite.selectSQL(dbName(), query);
	}

}
