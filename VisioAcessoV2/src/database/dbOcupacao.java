package database;

import objetos.Ocupacao;

public class dbOcupacao extends bancoDados {

	public static final String DB = "visio.db";
	public static final String TABLE = "OCUPACAO";

	public static final int ID = 0;
	public static final int IDCABO = 1;
	public static final int FIBRA = 2;
	public static final int IDCLIENTE = 3;
	public static final int QUANT = 4;

	public Ocupacao item;

	public void populate(Ocupacao ocupacao) {
		item = ocupacao;
	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case IDCABO:
			return "IDCABO";
		case FIBRA:
			return "FIBRA";
		case IDCLIENTE:
			return "IDCLIENTE";
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER NOT NULL";
		case IDCABO:
			return "INTEGER NOT NULL";
		case FIBRA:
			return "INTEGER NOT NULL";
		case IDCLIENTE:
			return "INTEGER NOT NULL";
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case ID:
			return SQLite.toText(item.getId());
		case IDCABO:
			return SQLite.toText(item.getIdCabo());
		case FIBRA:
			return SQLite.toText(item.getFibra());
		case IDCLIENTE:
			return SQLite.toText(item.getIdCliente());
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

}
