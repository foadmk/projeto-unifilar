package database;

import objetos.Local;

public class dbLocal extends bancoDados {

	public static final String DB = "visio.db";
	public static final String TABLE = "LOCAIS";

	public static final int ID = 0;
	public static final int UTMX = 1;
	public static final int UTMY = 2;
	public static final int ENDERECO = 3;
	public static final int BAIRRO = 4;
	public static final int CIDADE = 5;
	public static final int QUANT = 6;

	//private long id;

	//private int utmX, utmY;

	//private String endereco, bairro, cidade;
	
	public Local item;

	public void populate(Local local) {
		item = local;
	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case UTMX:
			return "UTMX";
		case UTMY:
			return "UTMY";
		case ENDERECO:
			return "ENDERECO";		
		case BAIRRO:
			return "BAIRRO";		
		case CIDADE:
			return "CIDADE";				
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER NOT NULL";
		case UTMX:
			return "INTEGER NOT NULL";
		case UTMY:
			return "INTEGER NOT NULL";
		case ENDERECO:
			return "STRING NOT NULL";
		case BAIRRO:
			return "STRING NOT NULL";			
		case CIDADE:
			return "STRING NOT NULL";				
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case ID:
			return SQLite.toText(item.getId());
		case UTMX:
			return SQLite.toText(item.getUtmX());
		case UTMY:
			return SQLite.toText(item.getUtmY());
		case ENDERECO:
			return SQLite.toText(item.getEndereco());
		case BAIRRO:
			return SQLite.toText(item.getBairro());	
		case CIDADE:
			return SQLite.toText(item.getCidade());				
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

}
