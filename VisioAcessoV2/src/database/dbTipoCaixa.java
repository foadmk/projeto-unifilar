package database;

import objetos.TipoCaixa;

public class dbTipoCaixa extends bancoDados {

	public static final String DB = "visio.db";
	public static final String TABLE = "TIPOCAIXA";

	public static final int ID = 0;
	public static final int NOME = 1;
	public static final int SIMBOLO = 2;
	public static final int QUANT = 3;


	public TipoCaixa item;

	public void populate(TipoCaixa tipoCaixa) {
		item = tipoCaixa;
	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case NOME:
			return "NOME";
		case SIMBOLO:
			return "SIMBOLO";
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER NOT NULL";
		case NOME:
			return "STRING NOT NULL";
		case SIMBOLO:
			return "INTEGER NOT NULL";
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case ID:
			return SQLite.toText(item.getId());
		case NOME:
			return SQLite.toText(item.getNome());
		case SIMBOLO:
			return SQLite.toText(item.getSimbolo());
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

}
