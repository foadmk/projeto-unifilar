package database;

import objetos.Cliente;

public class dbCliente extends bancoDados {

	public static final String DB = "visio.db";
	public static final String TABLE = "CLIENTES";

	public static final int ID = 0;
	public static final int NOME = 1;
	public static final int POSX = 2;
	public static final int POSY = 3;
	public static final int CIRCUITOS = 4;
	public static final int IDLOCAL = 5;
	public static final int QUANT = 6;


	public Cliente item;

	public void populate(Cliente cliente) {
		item = cliente;
	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case NOME:
			return "NOME";
		case POSX:
			return "POSX";
		case POSY:
			return "POSY";		
		case CIRCUITOS:
			return "CIRCUITOS";		
		case IDLOCAL:
			return "IDLOCAL";				
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER NOT NULL";
		case NOME:
			return "STRING NOT NULL";
		case POSX:
			return "INTEGER NOT NULL";
		case POSY:
			return "INTEGER NOT NULL";
		case CIRCUITOS:
			return "STRING NOT NULL";			
		case IDLOCAL:
			return "INTEGER NOT NULL";				
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case ID:
			return SQLite.toText(item.getId());
		case NOME:
			return SQLite.toText(item.getNome());
		case POSX:
			return SQLite.toText(item.getPosX());
		case POSY:
			return SQLite.toText(item.getPosY());
		case CIRCUITOS:
			return SQLite.toText(item.getCircuitos());	
		case IDLOCAL:
			return SQLite.toText(item.getIdLocal());				
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

}
