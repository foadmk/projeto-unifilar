package database;

import objetos.TipoCabo;

public class dbTipoCabo extends bancoDados {

	public static final String DB = "visio.db";
	public static final String TABLE = "TIPOCABO";

	public static final int ID = 0;
	public static final int NOME = 1;
	public static final int NUMFIBRAS = 2;
	public static final int QUANT = 3;


	public TipoCabo item;

	public void populate(TipoCabo tipoCabo) {
		item = tipoCabo;
	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case NOME:
			return "NOME";
		case NUMFIBRAS:
			return "NUMFIBRAS";
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER NOT NULL";
		case NOME:
			return "STRING NOT NULL";
		case NUMFIBRAS:
			return "INTEGER NOT NULL";
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case ID:
			return SQLite.toText(item.getId());
		case NOME:
			return SQLite.toText(item.getNome());
		case NUMFIBRAS:
			return SQLite.toText(item.getNumeroFibras());
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

}
