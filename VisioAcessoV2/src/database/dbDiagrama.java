package database;

import java.util.Vector;

import objetos.Diagrama;

public class dbDiagrama extends bancoDados {

	public static final String DB = "visio.db";
	public static final String TABLE = "DIAGRAMA";

	public static final int ID = 0;
	public static final int NOME = 1;
	public static final int IDCABOS = 2;	
	public static final int QUANT = 3;


	public Diagrama item;

	public void populate(Diagrama diagrama) {
		item = diagrama;
	}

	@Override
	public String sqlName(int var) {
		switch (var) {
		case ID:
			return "ID";
		case NOME:
			return "NOME";
		case IDCABOS:
			return "IDCABOS";
		}
		return null;
	}

	@Override
	public String sqlType(int var) {
		switch (var) {
		case ID:
			return "INTEGER NOT NULL";
		case NOME:
			return "STRING NOT NULL";
		case IDCABOS:
			return "TEXT NOT NULL";
		}
		return null;
	}

	@Override
	public String sqlValue(int var) {
		switch (var) {
		case ID:
			return SQLite.toText(item.getId());
		case NOME:
			return SQLite.toText(item.getNome());
		case IDCABOS:
			Vector<String> cabos = item.getIdCabos();
			String idCabos = "";
			for(String cabo : cabos){
				idCabos += cabo + ";";
			}
			return idCabos;
		}
		return null;
	}

	@Override
	public int sqlQuant() {
		return QUANT;
	}

	@Override
	public String dbName() {
		return DB;
	}

	@Override
	public String tableName() {
		return TABLE;
	}

}
